const ProxyList = require('free-proxy');
const proxyList = new ProxyList();

async function getProxyList() {
    let proxies;
    try {
        proxies = await proxyList.get();
        console.log('proxies', proxies)
    } catch (error) {
        throw new Error(error);
    }
}
getProxyList()