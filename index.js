const sharp = require('sharp');
const fs = require('fs');
var axios = require('axios');


async function readData () {
    try {
        var start = new Date().getTime();
        const url = "https://obs.mtlab.meitu.com/mtopen/VJ5hQlfjou7Q9qRokCyrP9KkoZjTbxBG/MTYzOTM1NzIwMA==/689888fb-8349-4012-ae74-f6cda34dda41.jpg";
        const image = await axios.get(url, { responseType: 'arraybuffer' });
        // const raw = Buffer.from(image.data).toString('base64');
        const raw = Buffer.from(image.data)
        sharp(raw)
            // .resize(1900,1900)
            .composite([
                {
                    input: "background.jpeg",
                    top: 134,
                    left: 116,
                },
                {
                    input: "background.jpeg",
                    top: 2132,
                    left: 133,
                },
            ])
            .toBuffer()
            .then(data => {
                fs.writeFileSync('yellow.png', data);
                var end = new Date().getTime();
                var time = end - start;
                console.log('Execution time: ' + time);
            })
            .catch(err => {
                console.log(err);
            });
    }catch(e) {
        console.log('e',e)
    }
   
} 
// const sharp = require("sharp");

async function addTextOnImage() {
    try {
        const width = 600;
        const height = 200;
        const text = "Sammy the Shark";

        const svgImage = `
 <svg version="1.0" xmlns="http://www.w3.org/2000/svg"
  width="${width}" height="${height}" viewBox="0 0 ${width}.000000 ${height}.000000"
 preserveAspectRatio="xMidYMid meet">

<g transform="translate(0.000000,200.000000) scale(0.100000,-0.100000)"
fill="#000000" stroke="none">
<path d="M0 970 l0 -970 1295 0 1295 0 0 970 0 970 -1295 0 -1295 0 0 -970z
m2465 5 l0 -830 -1162 0 c-639 0 -1164 3 -1167 6 -5 4 -10 1376 -6 1637 l0 22
1168 -2 1167 -3 0 -830z"/>
<path d="M245 1783 c-47 -2 -82 -9 -87 -16 -4 -7 -8 -366 -8 -798 l0 -787 23
-6 c12 -3 522 -6 1134 -6 1003 0 1113 2 1127 16 15 14 16 94 14 794 -2 623 -6
782 -16 792 -10 10 -215 13 -1060 14 -576 0 -1083 -1 -1127 -3z"/>
</g>
</svg>
    `;
        const svgBuffer = Buffer.from(svgImage);
        const image = await sharp("meitu.jpeg")
            .composite([
                {
                    input: svgBuffer,
                    top: 134,
                    left: 116,
                },
                {
                    input: svgBuffer,
                    top: 2132,
                    left: 133,
                },
            ])
            .toFile("sammy-text-overlay.png");
    } catch (error) {
        console.log(error);
    }
}

addTextOnImage()