const puppeteer = require("puppeteer-extra");
const cron = require('node-cron');
const fetch = require('node-fetch');
const url ="https://ai.meitu.com/algorithm/faceTechnology/skinanlysis?t=1579531178276";
const url_taskMap = {};
var DEBUG = false;
// ENABLE/DISABLE Console Logs
if(!DEBUG){
  console.log = function() {}
}

// turn off executablePath: "/usr/bin/chromium-browser" when run local
async function StartScraping() {
    try {
        let keyAuth = ''
        await puppeteer
            .launch({
                headless: true,
                // executablePath: "/usr/bin/chromium-browser",
                args: ['--no-sandbox']
            })
            .then(async (browser) => {
                const page = await browser.newPage();
    
                await page.setViewport({
                    width: 1366,
                    height: 768,
                });
                let index = 0
                page.on("response", async (response) => {
                    // console.log(await response);
                    // console.log(await response._request._resourceType);
                    // if (response._request._resourceType == "xhr") {
                    //   console.log(await response);
                    // }
                    if (response._request._resourceType == "xhr" && response.url().includes("https://openapi.mtlab.meitu.com/v2/skin")) {
                        console.log('console----' + index++)
                        let res = await response
                        keyAuth = res._request._headers.authorization
                        console.log('res', res._request._headers.authorization)
                        if (keyAuth !== '') {
                            console.log('keyAuth', keyAuth)
                            postAPI(keyAuth)
                        }
                    }
                });
    
                await page.goto(url, {
                    waitUntil: "load",
                    timeout: 0,
                });
                await page.waitFor(1000* 60 * 2);
                await page.close();
                await browser.close();
                console.log('close')
            });
     
    }catch(e) {
        console.log('e',e)
    }
   
}

function cronJob () {
    try {
        const task = cron.schedule('*/5 * * * *', async function () {
            console.log('cron job')
            await StartScraping();
        });
        url_taskMap[url] = task;
    }catch(e) {
        console.log('e', e)
    }

    // setTimeout(()=> {
    //     let my_job = url_taskMap[url];
    //     my_job.stop();   
    //     console.log('close job')
    // },1000*60*12)
  
}

function postAPI (key = '') {
    let data = {
        key: key
    };
    fetch('https://csgadmin.com/api/keyMeitu', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' }
    }).then(res => res.json())
        .then(json => console.log(json));
}

cronJob()
// StartScraping()