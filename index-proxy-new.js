const ProxyList = require('free-proxy');
const puppeteer = require('puppeteer-extra');
const pluginProxy = require('puppeteer-extra-plugin-proxy');
const url = "http://ai.meitu.com/algorithm/imageProcessing/makeup?t=1639981897441";




async function getProxyList() {
    let proxies = [];
    try {
        const proxyList = new ProxyList();
        proxies = await proxyList.get({
            browser: {
		headless: true,
		slowMo: 0,
		timeout: 10000,
            }, countries: ['us', 'vn','cn'], 
        });
        console.log('proxies', proxies)
        return proxies
    } catch (error) {
        console.log('error', error)
        return []
    }
}
async function StartScraping() {
    try {
        let proxyData = await getProxyList()
        if (proxyData.length > 0) {
            var item = proxyData[Math.floor(Math.random() * proxyData.length)];
            console.log('item', item.ip, item.port)
            puppeteer.use(pluginProxy({
                address: '14.241.225.167',
                port: 8080
            }));
            // puppeteer.use(pluginProxy({
            //     address: item.ip,
            //     port: item.port
            // }));
        }

        let keyAuth = ''
        await puppeteer
            .launch({
                headless: false,
            })
            .then(async (browser) => {
                const page = await browser.newPage();

                await page.setViewport({
                    width: 1366,
                    height: 768,
                });
                let index = 0
                page.on("response", async (response) => {
                    // console.log(await response);
                    // console.log(await response._request._resourceType);
                    // if (response._request._resourceType == "xhr") {
                    //   console.log(await response);
                    // }
                    if (response._request._resourceType == "xhr" && response.url().includes("https://openapi.mtlab.meitu.com/v3/makeup")) {
                        console.log('console----' + index++)
                        let res = await response
                        keyAuth = res._request._headers.authorization
                        console.log('res', res._request._headers.authorization)
                    }
                });

                await page.goto(url, {
                    waitUntil: "load",
                    timeout: 0,
                });
                await page.waitFor(5000);
                await page.close();
                await browser.close();
            });
        if (keyAuth !== '') {
            console.log('keyAuth', keyAuth)
        }
    }catch(e) {
        console.log('e',e)
    }
   
}
StartScraping();