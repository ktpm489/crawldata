const puppeteer = require("puppeteer-extra");

const url ="http://ai.meitu.com/algorithm/imageProcessing/makeup?t=1639981897441";
// const url ="https://ai.meitu.com/algorithm/faceTechnology/skinanlysis?t=1579531178276";

async function StartScraping() {
    let keyAuth = ''
    await puppeteer
        .launch({
            headless: false,
        })
        .then(async (browser) => {
            const page = await browser.newPage();

            await page.setViewport({
                width: 1366,
                height: 768,
            });
            let index = 0
            page.on("response", async (response) => {
                // console.log(await response);
                // console.log(await response._request._resourceType);
                // if (response._request._resourceType == "xhr") {
                //   console.log(await response);
                // }
                if (response._request._resourceType == "xhr" && response.url().includes("https://openapi.mtlab.meitu.com/v3/makeup")) {
                    console.log('console----' + index++)
                    let res = await response
                    keyAuth = res._request._headers.authorization
                    console.log('res', res._request._headers.authorization)
                }
            });

            await page.goto(url, {
                waitUntil: "load",
                timeout: 0,
            });
            // await page.waitFor(10000);
            // await page.close();
            // await browser.close();
        });
    if (keyAuth !== '') {
        console.log('keyAuth', keyAuth)
    }
}
StartScraping();