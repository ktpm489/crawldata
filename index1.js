const tesseract = require("node-tesseract-ocr")
const img = "https://obs.mtlab.meitu.com/mtopen/VJ5hQlfjou7Q9qRokCyrP9KkoZjTbxBG/MTYzOTM1NzIwMA==/689888fb-8349-4012-ae74-f6cda34dda41.jpg"
const config = {
    lang: "chi_sim",
    oem: 3,
    psm: 10,
}
tesseract
    .recognize(img, config)
    .then((text) => {
        console.log("Result:", text)
    })
    .catch((error) => {
        console.log(error.message)
    })