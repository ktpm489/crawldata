const sharp = require('sharp');
const fs = require('fs');
var axios = require('axios');


async function readData () {
    try {
        var start = new Date().getTime();
        const url = "https://obs.mtlab.meitu.com/mtopen/VJ5hQlfjou7Q9qRokCyrP9KkoZjTbxBG/MTYzOTM1NzIwMA==/689888fb-8349-4012-ae74-f6cda34dda41.jpg";
        const image = await axios.get(url, { responseType: 'arraybuffer' });
        // const raw = Buffer.from(image.data).toString('base64');
        const raw = Buffer.from(image.data)
        sharp(raw)
            // .resize(1900,1900)
            .composite([
                {
                    input: "background.jpeg",
                    top: 134,
                    left: 116,
                },
                {
                    input: "background.jpeg",
                    top: 2132,
                    left: 133,
                },
            ])
            .toBuffer()
            .then(data => {
                fs.writeFileSync('yellow.png', data);
                var end = new Date().getTime();
                var time = end - start;
                console.log('Execution time: ' + time);
            })
            .catch(err => {
                console.log(err);
            });
    }catch(e) {
        console.log('e',e)
    }
   
} 
// const sharp = require("sharp");

async function addTextOnImage() {
    try {
        const width = 340;
        const height = 97;
        const text = "Sammy the Shark";

        const svgImage = `
    <svg width="${width}" height="${height}">
      <style>
      .title { fill: #001; font-size: 20px; font-weight: bold;}
      </style>
      <text x="50%" y="50%" text-anchor="middle" class="title">${text}</text>
    </svg>
    `;
        const svgBuffer = Buffer.from(svgImage);
        const image = await sharp("meitu.jpeg")
            .composite([
                {
                    input: svgBuffer,
                    top: 134,
                    left: 116,
                },
                {
                    input: svgBuffer,
                    top: 2132,
                    left: 133,
                },
            ])
            .toFile("sammy-text-overlay.png");
    } catch (error) {
        console.log(error);
    }
}

addTextOnImage()