const puppeteer = require("puppeteer-extra");

const url ="https://justpaste.it/edit/45116656/h2r49bnbd9lxzhlo";

async function StartScraping() {
    await puppeteer
        .launch({
            headless: false,
        })
        .then(async (browser) => {
            const page = await browser.newPage();

            await page.setViewport({
                width: 1366,
                height: 768,
            });
            let index = 0
            page.on("response", async (response) => {
                // console.log(await response);
                // console.log(await response._request._resourceType);
                if (response._request._resourceType == "xhr") {
                  console.log(await response);
                }
              
            });

            await page.goto(url, {
                waitUntil: "load",
                timeout: 0,
            });
            // await page.waitFor(10000);
            // await page.close();
            // await browser.close();
        });
}
StartScraping();